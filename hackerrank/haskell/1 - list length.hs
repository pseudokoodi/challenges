-- "Count the number of elements in an array without using count, size or length operators (or their equivalents)"

len :: [a] -> Int
len lst = sum [ 1 | _ <- lst ]