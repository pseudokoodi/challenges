-- "Filter a given array of integers and output only those values that are less than a specified value X. 
-- The output integers should be in the same sequence as they were in the input. 
-- You need to write a function with the recommended method signature for the languages mentioned below."

f :: Int -> [Int] -> [Int]
f n arr = filter (< n) arr