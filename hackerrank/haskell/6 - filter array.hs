-- "For a given list with N integers, return a new list removing the elements at odd positions"

f :: [Int] -> [Int]
f lst = case drop 1 lst of
            (x:xs) -> x : f xs
            [] -> []