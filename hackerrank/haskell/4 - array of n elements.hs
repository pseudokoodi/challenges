-- "Create an array of n integers, where the value of n is 
-- passed as an argument to the pre-filled function in your editor."

fn n = [1 .. n]