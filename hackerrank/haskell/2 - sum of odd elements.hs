-- "You are given a list. Return the sum of odd elements from the given list"

f :: [Int] -> Int
f list = sum [ x | x <- list, mod x 2 /= 0]