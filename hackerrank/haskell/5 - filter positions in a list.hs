-- "For a given list with N integers, return a new list removing the elements at odd positions. 
-- The input and output portions will be handled automatically. 
-- You need to write a function with the recommended method signature."

f :: [Int] -> [Int]
f lst = case drop 1 lst of
            (x:xs) -> x : f xs
            [] -> []