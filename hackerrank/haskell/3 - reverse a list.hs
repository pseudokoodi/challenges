-- "You are given a list of N elements. Reverse the list without using the reverse function"

rev e
    | l > 0 = [last e] ++ rev (init e)
    | otherwise = [last e]
    where l = length e - 1