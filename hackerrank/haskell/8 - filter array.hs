-- "Given a list, repeat each element in the list n amount of times"

f :: Int -> [Int] -> [Int]
f n arr = concat [ [x | x <- xs ] | xs <- lists ]
        where lists = [ take n (repeat x) | x <- arr ]