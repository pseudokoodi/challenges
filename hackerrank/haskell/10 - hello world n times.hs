-- "Print "Hello World" amount of times."

hello_worlds n =    mapM_ putStrLn hellos
                    where hellos = take n (repeat "Hello World")