-- "Update the values of a list with their absolute values"

f :: [Int] -> [Int]
f list = [ if x >= 0 then x else x*(-1) | x <- list ]